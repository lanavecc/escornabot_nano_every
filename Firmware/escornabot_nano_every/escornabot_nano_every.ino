#include "escornabot.h"


int acciones[20];
escornabot mirobot;
int pulsaciones = 0;
int n;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
    mirobot.buzzON();
    mirobot.ledON(5);
    delay (200);
    mirobot.buzzOFF();
    mirobot.ledOFF(5);
}

void loop() {
  // put your main code here, to run repeatedly:
  if (mirobot.pushButton() == forward) {
    acciones[pulsaciones] = 1;
    pulsaciones = pulsaciones + 1;
    mirobot.ledON (1);
    mirobot.buzzON();
    delay (200);
    mirobot.ledOFF (1);
    mirobot.buzzOFF();
  }
  else if (mirobot.pushButton() == backward) {
    acciones[pulsaciones] = 2;
    pulsaciones = pulsaciones + 1;
    mirobot.ledON (3);
    mirobot.buzzON();
    delay (200);
    mirobot.ledOFF (3);
    mirobot.buzzOFF();
  }
  else if (mirobot.pushButton() == left) {
    acciones[pulsaciones] = 3;
    pulsaciones = pulsaciones + 1;
    mirobot.ledON (2);
    mirobot.buzzON();
    delay (200);
    mirobot.ledOFF (2);
    mirobot.buzzOFF();
  }
  else if (mirobot.pushButton() == right) {
    acciones[pulsaciones] = 4;
    pulsaciones = pulsaciones + 1;
    mirobot.ledON (4);
    mirobot.buzzON();
    delay (200);
    mirobot.ledOFF (4);
    mirobot.buzzOFF();
  }
  else if (mirobot.pushButton() == central) {
    mirobot.buzzON();
    mirobot.ledON(5);
    delay (200);
    mirobot.buzzOFF();
    mirobot.ledOFF(5);
    for (n = 0; n < pulsaciones; n++) {
      int led = 0;
      if (acciones[n] == 1) {
        led = 1;
        mirobot.buzzON();
        mirobot.ledON(led);
        delay (20);
        mirobot.buzzOFF();
        mirobot.driveD(10, 12); 
      }
      else if (acciones[n] == 2) {
        led = 3;
        mirobot.buzzON();
        mirobot.ledON(led);
        delay (20);
        mirobot.buzzOFF();
        mirobot.driveD(-10, 12);
      }
      else if (acciones[n] == 3) {
        led = 2;
        mirobot.buzzON();
        mirobot.ledON(led);
        delay (20);
        mirobot.buzzOFF();
        mirobot.turnA(-90, 12);
      }
      else if (acciones[n] == 4) {
        led = 4;
        mirobot.buzzON();
        mirobot.ledON(led);
        delay (20);
        mirobot.buzzOFF();
        mirobot.turnA(90, 12);
      }
      Serial.println (acciones[n]);
      mirobot.ledOFF(led);
    }
    delay (200);
    pulsaciones = 0;
    mirobot.buzzON();
    mirobot.ledON(5);
    delay (300);
    mirobot.buzzOFF();
    mirobot.ledOFF(5);
  }


}
